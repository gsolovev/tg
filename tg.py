from scapy.all import sendp,IP,UDP,TCP,Ether
import argparse
import random
import config
import sys

tc_cfg = config.traffic_classes


def send_packet(tc, count, dst_mac, iface):

    print(count)
    dst_ip = tc_cfg[str(tc)]["dst_ip"]
    src_ip = tc_cfg[str(tc)]["src_ip"]
    proto = tc_cfg[str(tc)]["proto"]
    dst_port = tc_cfg[str(tc)]["dst_port"]
    padding = "x"*128

    pkt = Ether(dst=dst_mac) / IP(src=src_ip, dst=dst_ip)
    if proto == "tcp":
        pkt /= TCP(sport=random.randint(1025,65535), dport=dst_port) / padding
    elif proto == "udp":
        pkt /= UDP(sport=random.randint(1025,65535), dport=dst_port) / padding
    else:
        print("PROTO is not udp or tcp")
        sys.exit(1)
    
    sendp(pkt, iface=iface, count=count, inter=0.1)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-tc", dest="tc", required=True, help="Traffic class, it can be from 0 to 7")
    parser.add_argument("-c", dest="count", default=1, type=int, help="Number of sending packets. Default is 1")
    parser.add_argument("-d", dest="dst_mac", required=True, type=str, help="MAC address of default gateway")
    parser.add_argument("-if", dest="iface", required=True, type=str, help="Source interface")

    args = parser.parse_args()

    send_packet(args.tc, args.count, args.dst_mac, args.iface)


if __name__ == "__main__":
    main()