# Simple traffic generator by Scapy

## Установка под Windows

1. Качаем инсталлятор Python 3.9.x - https://www.python.org/downloads/release/python-397/
2. Устанавливаем, не забыв выбрать опцию "Add Python 3.9 to PATH"
3. Скачиваем и устанавливаем Npcap https://nmap.org/npcap/ (идет в комплекте с wireshark) или Winpcap https://www.winpcap.org/install/default.htm 
4. Скачиваем и устанавливаем Git - https://git-scm.com/download/win, все настройки оставляем по умолчанию
5. Открываем PowerShell и вводим команду: `pip install scapy`
6. Клонируем репозиторий в локальную папку: `git clone https://gitlab.com/gsolovev/tg.git`
7. переходим в папку со скриптом: `cd tg`
8. Если необходимо изменяем конфигурационный файл - [config.py](config.py) в текстовом редакторе, например Блокноте (Notepad)

## Запускаем скрипт

Открываем PowerShell и переходим в папку со скриптом.

Справка по скрипту просматривается командой:
```
python tg.py -h
usage: tg.py [-h] -tc TC [-c COUNT] -d DST_MAC -if IFACE

optional arguments:
  -h, --help  show this help message and exit
  -tc TC      Traffic class, it can be from 0 to 7
  -c COUNT    Number of sending packets. Default is 1
  -d DST_MAC  MAC address of default gateway
  -if IFACE   Source interface
```

Пример запуска: 

```
python tg.py -tc 0 -d "00:00:00:00:00:01" -if "Ethernet0"
```

